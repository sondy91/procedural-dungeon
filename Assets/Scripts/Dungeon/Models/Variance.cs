﻿using System;

namespace Scripts.Dungeon.Models
{
	public enum Variance
	{
		None = 0,

		VeryLow = 1,

		Low = 2,

		Medium = 3,

		High = 4,

		VeryHigh = 5,

		Custom = 6
	}
}

