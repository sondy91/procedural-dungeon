﻿using System;

namespace Scripts.Dungeon.Models
{
	using Random = UnityEngine.Random;

	public class Variant
	{
		public int Value { get; set; }
		public Variance Variance { get; set; }

		internal Variant (int value, Variance? variance)
		{
			Value = VaryValue(value, variance.GetValueOrDefault());
			Variance = variance.GetValueOrDefault();
		}

		internal Variant(int min, int max)
		{
			Value = Random.Range(min, max);
			Variance = Variance.Custom;
		}

		private int VaryValue(int value, Variance variance)
		{
			if(variance == Variance.None)
			{
				return value;
			}

			if(value < 10)
			{
				return VarySmallValue(value, variance);
			}
			return 0;
			//mod 10 the value, that way we can get the degree to set the modifier
			//For each variance, use that modifier degree to pick a range up to a certain percent
		}

		//Handles values under 10
		private int VarySmallValue(int value, Variance variance)
		{
			
			if(variance == Variance.VeryLow) 
			{
				var mod = Random.Range(0,1);
				return Modify(value, mod);
			}
			else if(variance == Variance.Low)
			{
				var mod = Random.Range(0, 2);
				return Modify(value, mod);
			}
			else if(variance == Variance.Medium)
			{
				var mod = Random.Range(0, 3);
				return Modify(value, mod);
			}
			else if(variance == Variance.High)
			{
				var mod = Random.Range(0, 5);
				return Modify(value, mod);
			}
			else
			{
				var mod = Random.Range(0, 8);
				return Modify(value, mod);
			}

		}

		//Decides whether to add or safely subtract the modifier from the value given
		private int Modify(int value, int mod)
		{
			Boolean AddModToValue = Random.Range(0, 1) != 0;

			if(AddModToValue)
			{
				return value + mod;
			}

			if(mod == value)
			{
				return 1;
			}
			else if(mod > value)
			{
				return mod - value;
			}
			else
			{
				return value - mod;
			}
		}

	}
}
