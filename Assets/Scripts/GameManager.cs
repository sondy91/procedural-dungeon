﻿namespace Scripts
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Dungeon;
	using Dungeon.Models;

	public class GameManager : MonoBehaviour 
	{

		public static GameManager instance = null;
		private DungeonGenerator dungeonGenerator;
		private DungeonOptions options;

		void Awake() 
		{
			if(instance == null)
			{
				instance = this;
			}
			else if (instance != this)
			{
				Destroy (gameObject);
			}

			DontDestroyOnLoad (gameObject);
			dungeonGenerator = GetComponent<DungeonGenerator> ();
			InitGame ();
		}


		// Use this for initialization
		void InitGame () 
		{
			//options = new DungeonOptions() { RoomCount = new Variant<int>(4, Variance.None) };
			options = GenerateDungeonOptions();
			dungeonGenerator.GenerateDungeon (options);
		}

		// Update is called once per frame
		void Update () {

		}

		//Probably go random with this eventually 
		private DungeonOptions GenerateDungeonOptions()
		{
			return new DungeonOptions () 
			{
				RoomCount = new Variant<int>(4, Variance.None)
			};
		}
	}
}
